# Basic CRUD TO DO LIST

## Prerequisites  

- NestJS
- Node 20.14.0

## Description

REST API โดยใช้ Node JS (JavaScript or TypeScript) for To do list without database

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

### PostMan
 
directory : /docs/REST API basics- Todo list.postman_collection.json


### CRUD

1. Get Todo List

```
curl --location 'http://localhost:3000/to-do-list'
```

2. Get Todo By Id

```
curl --location 'http://localhost:3000/to-do-list/:id'
```

3. Create Todo

```
curl --location 'http://localhost:3000/to-do-list' \
--header 'Content-Type: application/json' \
--data '{
    "title": "Add your name in the body",
    "description": "description",
    "completed": true
}'
```

4. Update Todo

```
curl --location --request PATCH 'http://localhost:3000/to-do-list/a22ccc38-17a5-494c-aeb9-c3ce8bfd94c4' \
--header 'Content-Type: application/json' \
--data '{
    "title": "Add your name in the body update",
    "description": "description",
    "completed": true
}'
```


