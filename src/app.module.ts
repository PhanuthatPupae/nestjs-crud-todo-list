import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ToDoListModule } from './to-do-list/to-do-list.module';
@Module({
  imports: [
    ToDoListModule,

  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
