import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ToDoListService } from './to-do-list.service';
import { CreateToDoListDto } from './dto/create-to-do-list.dto';
import { UpdateToDoListDto } from './dto/update-to-do-list.dto';

@Controller('to-do-list')
export class ToDoListController {
  constructor(private readonly toDoListService: ToDoListService) { }

  @Post()
  create(@Body() createToDoListDto: CreateToDoListDto) {
    //Mock code 200 for ever
    return {
      code: 200,
      data: this.toDoListService.create(createToDoListDto)
    }
  }

  @Get()
  findAll() {
    //Mock code 200 for ever
    return {
      code: 200,
      data: this.toDoListService.findAll(),
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return {
      code: 200,
      data: this.toDoListService.findOne(id)
    }
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateToDoListDto: UpdateToDoListDto) {
    return {
      code: 200,
      data: this.toDoListService.update(id, updateToDoListDto)
    }
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return {
      code: 200,
      data: this.toDoListService.remove(id)
    }
  }
}
