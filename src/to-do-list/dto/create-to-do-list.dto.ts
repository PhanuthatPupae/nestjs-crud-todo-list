export class CreateToDoListDto {
    readonly title: string;
    readonly description: string;
    readonly completed: boolean;
}
