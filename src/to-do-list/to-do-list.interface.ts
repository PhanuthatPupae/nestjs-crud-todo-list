export interface ToDoList {
    id: string;
    title: string;
    description: string;
    completed: boolean;
    created_at: Date;
    updated_at: Date;
    deleted_at?: Date;
}
