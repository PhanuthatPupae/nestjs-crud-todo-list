import { Injectable } from '@nestjs/common';
import { CreateToDoListDto } from './dto/create-to-do-list.dto';
import { UpdateToDoListDto } from './dto/update-to-do-list.dto';
import { ToDoList } from './to-do-list.interface';
import { uuid } from 'uuidv4';
@Injectable()
export class ToDoListService {
  private todoList: ToDoList[] = [];

  create(createToDoListDto: CreateToDoListDto) {
    const newTodo: ToDoList = {
      id: uuid(),
      created_at: new Date(),
      updated_at: new Date(),
      deleted_at: null,
      ...createToDoListDto,

    };
    this.todoList.push(newTodo);
    return newTodo;
  }

  findAll() {
    return this.todoList
  }

  findOne(id: string) {
    return this.todoList.find(todo => todo.id === id);
  }

  update(id: string, updateToDoListDto: UpdateToDoListDto) {
    const todoIndex = this.todoList.findIndex(todo => todo.id === id);
    if (todoIndex > -1) {
      const prevData = this.todoList[todoIndex]
      const updateTodo: ToDoList = {
        ...prevData,
        ...updateToDoListDto,
        updated_at: new Date(),

      };
      this.todoList[todoIndex] = updateTodo
      return this.todoList[todoIndex];
    }
    return null;
  }

  remove(id: string) {
    this.todoList = this.todoList.map(todo => {
      if (todo.id == id) {
        return {
          ...todo,
          deleted_at: new Date()
        }
      } else {
        return { ...todo }
      }
    });
  }
}
